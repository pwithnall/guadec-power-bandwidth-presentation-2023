\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\hypersetup{bookmarks=true, colorlinks=false, linkcolor=blue, citecolor=blue, filecolor=blue, pagecolor=blue, urlcolor=blue,
            pdftitle=Reducing power and bandwidth use in apps to keep users happy,
            pdfauthor=Philip Withnall, pdfsubject=, pdfkeywords=}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,automata,positioning,er,calc,decorations.pathmorphing,fit}
\usepackage{listings}
\usepackage{subfigure} % can't use subfig because it doesn't support Beamer
\usepackage{fancyvrb}
\usepackage{ulem}
\usepackage{siunitx}
\usepackage[backend=biber]{biblatex}

% Syntax highlighting colours from the Tango palette: http://pbunge.crimson.ch/2008/12/tango-syntax-highlighting/
\definecolor{LightButter}{rgb}{0.98,0.91,0.31}
\definecolor{LightOrange}{rgb}{0.98,0.68,0.24}
\definecolor{LightChocolate}{rgb}{0.91,0.72,0.43}
\definecolor{LightChameleon}{rgb}{0.54,0.88,0.20}
\definecolor{LightSkyBlue}{rgb}{0.45,0.62,0.81}
\definecolor{LightPlum}{rgb}{0.68,0.50,0.66}
\definecolor{LightScarletRed}{rgb}{0.93,0.16,0.16}
\definecolor{Butter}{rgb}{0.93,0.86,0.25}
\definecolor{Orange}{rgb}{0.96,0.47,0.00}
\definecolor{Chocolate}{rgb}{0.75,0.49,0.07}
\definecolor{Chameleon}{rgb}{0.45,0.82,0.09}
\definecolor{SkyBlue}{rgb}{0.20,0.39,0.64}
\definecolor{Plum}{rgb}{0.46,0.31,0.48}
\definecolor{ScarletRed}{rgb}{0.80,0.00,0.00}
\definecolor{DarkButter}{rgb}{0.77,0.62,0.00}
\definecolor{DarkOrange}{rgb}{0.80,0.36,0.00}
\definecolor{DarkChocolate}{rgb}{0.56,0.35,0.01}
\definecolor{DarkChameleon}{rgb}{0.30,0.60,0.02}
\definecolor{DarkSkyBlue}{rgb}{0.12,0.29,0.53}
\definecolor{DarkPlum}{rgb}{0.36,0.21,0.40}
\definecolor{DarkScarletRed}{rgb}{0.64,0.00,0.00}
\definecolor{Aluminium1}{rgb}{0.93,0.93,0.92}
\definecolor{Aluminium2}{rgb}{0.82,0.84,0.81}
\definecolor{Aluminium3}{rgb}{0.73,0.74,0.71}
\definecolor{Aluminium4}{rgb}{0.53,0.54,0.52}
\definecolor{Aluminium5}{rgb}{0.33,0.34,0.32}
\definecolor{Aluminium6}{rgb}{0.18,0.20,0.21}

% Increase the space after the footnotes so that \footfullcite works
\addtobeamertemplate{footline}{\hfill\usebeamertemplate***{navigation symbols}%
    \hspace*{0.1cm}\par\vskip 20pt}{}

\usetheme{guadec}

\AtBeginSection{\frame{\sectionpage}}


% Abstract here: https://events.gnome.org/event/101/contributions/454/
%
% Users who are on battery power, or using a mobile connection, can sometimes
% have a frustrating experience with apps which use lots of power, or keep
% downloading things. In some cases, this can lead to large bandwidth usage
% bills before the user realises.
%
% The GNOME platform provides ways to change app behaviour to save power and
% bandwidth when appropriate. This talk will go through some examples of them.
% The talk is aimed at app authors.


\title{Reducing power and bandwidth use in apps to keep users happy}

\author{Philip Withnall\\Endless\\\texttt{philip@tecnocode.co.uk}}
\date{July 26, 2023}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\note{25 minutes allocated. 20 minutes for talk, 5 minutes for questions.

Hello, today I’m going to talk about two things you might want to pay attention
to in your apps, how easy it is to do so, and some caveats to be aware of when
implementing them. I’m going to talk about bandwidth and metered data, and then
about power and power saving. For each, I’ll give the APIs you need to use, and
then some examples of how other apps make use of them.}


\begin{frame}{Networks}
\begin{itemize}
	\item{Networks can be metered}
	\item{Metered status is broadcast by the network, or set by the user}
	\item{Metered networks are generally slow or cost money per unit data}
\end{itemize}
\end{frame}

\note{Firstly, let’s talk about metered networks and saving bandwidth. A metered
network is one where there’s some cost per unit data, or some cap on the amount
of data you can use. Generally the metered status is advertised by the network
(using some special DHCP features), but sometimes it isn’t and has to be set
manually by the user. An example of a metered network would be tethered internet
from a phone, or a 4G modem.}

\note{How can you save data? By downloading less stuff, downloading it less
frequently, or not doing things altogether. For example, your app could check
for updated content less often; not automatically download images or generate
link previews; only download summaries of content and require the user to
explicitly request the full version of something if they want it; etc.}


\begin{frame}{Metered API}
\large{\href{https://docs.gtk.org/gio/iface.NetworkMonitor.html}{\texttt{Gio.NetworkMonitor.network-metered}}}
\end{frame}

\note{GLib provides an API for indicating whether the network connection is
metered, as a property, and as a \texttt{notify} signal for when that changes.
You will always want to listen to the signal, as users may change networks
several times during the lifetime of your app’s process.}

\note{So, determining whether the network is metered is as simple as checking
a property and connecting to a signal. Great! So you do that and disable or
throttle some functionality in your app. But what if the user was expecting
that functionality to work? It would be good to give them some feedback.}


\begin{frame}{Metered UI considerations}
\begin{figure}
	\includegraphics[width=0.75\columnwidth]{hig-feedback.png}
	\caption{The \href{https://developer.gnome.org/hig/patterns/feedback.html}{HIG page on UI feedback patterns}}
\end{figure}
\end{frame}

\note{The GNOME human interface guidelines (HIG) provide guidance on patterns
for feedback to the user. You can read the HIG online. Being on a metered
network is a temporary state, but it can be that way for a while. It’s also a
notable state transition when going from non-metered to metered, or vice-versa.
That gives us several options for how to notify the user.}


\begin{frame}{Metered UI considerations}
\begin{figure}
	\includegraphics[width=0.50\columnwidth]{hig-notifications.png}
	\caption{The \href{https://developer.gnome.org/hig/patterns/feedback/notifications.html}{HIG page on notifications}}
\end{figure}
\end{frame}

\note{Here’s what the HIG has to say about notifications. They’re useful for
when users are looking at another app, and you should be careful to not
needlessly distract the user with them.}


\begin{frame}{Metered UI considerations}
\begin{figure}
	\includegraphics[width=0.50\columnwidth]{hig-toasts.png}
	\caption{The \href{https://developer.gnome.org/hig/patterns/feedback/toasts.html}{HIG page on toasts}}
\end{figure}
\end{frame}

\note{This is the HIG guidance on toasts. They are useful in the context of an
app, often in response to a user action. They are transient and are therefore
best suited to communicating events rather than states.}


\begin{frame}{Metered UI considerations}
\begin{figure}
	\includegraphics[width=0.50\columnwidth]{hig-banners.png}
	%\caption{The \href{https://developer.gnome.org/hig/patterns/feedback/banners.html}{HIG page on banners}}
\end{figure}
\end{frame}

\note{The HIG guidance on banners. They are persistent and are used to
communicate persistent state. They are deliberately attention-grabbing, so
should only be used to communicate important states.}


\begin{frame}{Metered example: Fragments}
\begin{figure}
	\includegraphics[width=0.9\columnwidth]{fragments-infobar.png}
	\caption{Fragments showing an infobar on a metered network}
\end{figure}
\end{frame}

\begin{frame}{Metered example: Fragments}
\begin{itemize}
	\item{\url{https://gitlab.gnome.org/World/Fragments/-/merge_requests/156}}
	\item{Pause torrents when moving onto a metered network}
	\item{Resume them when moving off the metered network (unless the user has modified them since)}
	\item{Display an infobar while metered}
\end{itemize}
\end{frame}

\note{Let’s look at an example app which supports changing its behaviour when
on a metered network. Fragments is a torrent client, which might be running
with torrents downloading or seeding in the background. The user probably
doesn’t want to spend their precious metered bandwidth on those torrents, unless
they make an explicit choice to.}

\note{So, Fragments will detect the state transition for moving from a
non-metered to a metered network, and pause all torrents at that point. Since
the user probably expected those torrents to complete at some point, it will
resume the torrents again when moving from a metered to a non-metered network,
unless the user has explicitly modified the torrents in the meantime.}

\note{Throughout all of this, Fragments will show an infobar telling the user
that torrents were disabled because of being on a metered network. The HIG
recommends using infobars to communicate persistent state. To be really
up to date with recommended widgets, this should be a libadwaita banner rather
than a GTK infobar, but infobars are what’s currently in use in the rest of
Fragments. If a banner were used, the explanatory text should be shorter.}


\begin{frame}{Metered example: Déjà Dup}
\begin{figure}
	\includegraphics[width=0.85\columnwidth]{deja-dup-notification.png}
	\caption{Déjà Dup showing a notification when on a metered network}
\end{figure}
\end{frame}

\begin{frame}{Metered example: Déjà Dup}
\begin{itemize}
	\item{\url{https://gitlab.gnome.org/World/deja-dup/-/blob/main/libdeja/Network.vala\#L59}}
	\item{Doesn’t begin a backup while on a metered network}
	\item{\ldots{}unless a preference is set to override this}
\end{itemize}
\end{frame}

\note{Another example app is Déjà Dup, a backup program. It can be configured to
back up to a network location, which could use significant amounts of data. So,
if running on a metered network it will delay backups unless explicitly told to
run one by the user. The scheduling is overrideable with a hidden preference.}

\note{Since Déjà Dup runs in the background and does its backups on a schedule,
the user will almost never see its UI. So the only way to notify the user that
an expected backup has not run due to being on a metered network is a
desktop notification. Déjà Dup tracks when it has most recently notified the
user about a scheduled backup, so it doesn’t emit notifications too often.}

\note{Most apps should not use notifications to tell the user about being on a
metered network. Imagine what would happen if you were running 10 apps which
changed their behaviour on a metered network, and they all emitted a
notification when you moved from a non-metered to a metered network.
Notifications need to be well justified (and I think Déjà Dup’s use is
justified).}


\begin{frame}{Power}
\begin{itemize}
	\item{Traditionally restricted to ‘is the computer on battery?’}
	\item{Now a wider definition: power saver mode}
	\item{Can be explicitly enabled by the user in anticipation of being away from power for a long time}
\end{itemize}
\end{frame}

\note{In the second part of the talk, I’d like to talk about power saving.
Traditionally, apps have changed their behaviour based on whether the computer
is on battery power. But now we have a wider definition: is it in ‘power-saver’
mode. This can be set by being on battery, or by an explicit request from the
user, or by helper software.}

\note{Apps use power by using computer resources. In general, this means using
the CPU, using large amounts of memory, accessing disk or the network a lot
(particularly if the network is wireless), or just waking up frequently so the
processor can never get into a deeper sleep state. Saving power means doing
less of that, doing it less often, or grouping together things so that they’re
all done at once rather than waking up several times to do several things.}


\begin{frame}{Power API}
\large{\href{https://docs.gtk.org/gio/iface.PowerProfileMonitor.html}{\texttt{Gio.PowerProfileMonitor.power-saver-enabled}}}
\end{frame}

\note{GLib provides an API for indicating whether the computer is in power-saver
mode, and it’s quite similar to the metered data API. It’s a property plus a
\texttt{notify} signal, so all you need to do is read the property and connect
to the signal. You will want to listen to the signal, as the power mode may
change over time.}

\note{So if you use this API to disable, reduce or change some functionality in
your app -- as with metered data -- you need to be able to notify the user
somehow.}


\begin{frame}{Power UI considerations}
\begin{figure}
	\includegraphics[width=0.75\columnwidth]{hig-feedback.png}
	\caption{The \href{https://developer.gnome.org/hig/patterns/feedback.html}{HIG page on UI feedback patterns}}
\end{figure}
\end{frame}

\note{Just as with metered data, the HIG provides guidance on patterns for
feedback to the user. Being in power-saver mode is a temporary state, but it
can be that way for a while. It’s a less notable transition when going from
another power mode to power-saver; it’s more about the state here. This gives
us several options for how to notify the user.}

\note{As with any of the UI work here, follow the guidance in the HIG as it
applies to your app. If anything seems odd, or isn’t covered by the HIG, or you
want a second opinion, ask on
\href{https://matrix.to/\#/\#design:gnome.org}{the GNOME design channel on Matrix}.}


\begin{frame}{Power example: Blanket}
\begin{figure}
	\includegraphics[width=0.55\columnwidth]{blanket-toast.png}
	\caption{Blanket showing a toast when pausing sounds in power-saver mode}
\end{figure}
\end{frame}

\note{(The missing images here are an issue with how I’ve built it, and not an
issue with Blanket.)}

\begin{frame}{Power example: Blanket}
\begin{itemize}
  \item{\url{https://github.com/rafaelmardojai/blanket/pull/312}}
	\item{Pauses background playback when entering power saver mode}
	\item{Shows a toast explaining why playback has been paused}
	\item{In case Blanket’s UI is not visible, this toast doesn’t have a timeout}
\end{itemize}
\end{frame}

\note{Here’s an example of an app which changes its behaviour in power-saver
mode. Blanket is an app to play soothing background sounds, such as rain or
birdsong. When entering power-saver mode, it will pause the background sounds,
as continually doing audio decoding drains power.  It will display a toast in
its window to explain why the sounds have stopped, and the toast contains a
button to start playback again.}

\note{Blanket’s UI may not be visible (you can close the window and it will
continue playing), so the toast doesn’t have a timeout. If the user wonders why
the sounds have stopped, they will open Blanket’s window, and see the toast.
Another way to implement this would have been as a banner with a close button,
but that’s more of a way to represent an ongoing state, whereas pausing the
sound playback is a transitional change. Blanket doesn’t re-start playback when
the system changes away from power-saver mode, as suddenly playing sound without
user interaction is quite jarring.}


\begin{frame}{Power example: NewsFlash}
\begin{figure}
	\includegraphics[width=0.75\columnwidth]{newsflash-code.png}
	\caption{The code from NewsFlash to disable background syncing}
\end{figure}
\end{frame}

\begin{frame}{Power example: NewsFlash}
\begin{itemize}
  \item{\url{https://gitlab.com/news-flash/news_flash_gtk/-/merge_requests/148}}
	\item{Affects whether to enable background syncing of news feeds}
	\item{Adds to existing logic which disables them on metered networks}
	\item{No UI}
\end{itemize}
\end{frame}

\note{Another example is NewsFlash. This is a feed reader application. When in
power-saver mode, it will disable background refreshing of feeds, saving on
wakeups and network activity. Feeds will still be refreshed if the user
explicitly requests it.}

\note{The logic to implement this also disables refreshing of feeds when on a
metered network. Colocating both of these checks is quite common --- typically
measures taken to reduce network data use will also reduce power consumption.}

\note{Since it’s only background refreshes which change behaviour, NewsFlash
doesn’t need to notify the user at all. Seeing updates for their feeds now vs
in a few hours’ time will not really affect them.}


\begin{frame}{What haven’t I covered?}
\begin{itemize}
  \item{Profile your apps for power usage: reduce wakeups, reduce spinning}
  \item{\texttt{powertop} is amazing}
  \item{Profile your apps for network usage too: \texttt{nethogs}}
  \item{\texttt{Gio.MemoryMonitor.low-memory-warning}}
  \item{Don’t use web views or write your app using web technologies if you can help it, they’re terrible for power use}
  \item{Occasionally look at your project’s CI resource and GitLab disk use}
\end{itemize}
\end{frame}

\note{There’s a lot of other approaches to improving power and network use
which I haven’t covered. These things can be optimised in general, even when not
on a metered network or in power-saver mode. \texttt{powertop} and
\texttt{nethogs} are useful for seeing when your app is waking up or how much
data it’s using. There are many different techniques for optimising an
application, but they all come down to looking at the app’s behaviour first and
identifying problems.}

\note{Also, take a look at \texttt{Gio.MemoryMonitor}. It’s a similar API to
the two I’ve covered today, but for situations where the machine is running low
on memory. In these situations, your app should free up resources which it
doesn’t immediately need (making a CPU/memory/disk tradeoff). I’ve run out of
time to cover this though! Talk to me after for more information about it.}

\note{Another completely different area of resource use is development: we’ve
only covered resource use by users of your app, but sometimes development
resources can be more significant than we think. How often does your CI run?
Does it need to run that often? How many tens of gigabytes of project data are
you storing on GitLab, and is it all necessary? Or has some of it been
forgotten about?}

\note{So, if your app uses the internet, please add support for metered data
detection. If it could be changed to do less work when in power-saver mode,
please do that. And in any case, it would be great if you could take 10 minutes
to do some quick profiling with \texttt{powertop} and give your CI a checkup!}


\begin{frame}{Miscellany}
\begin{description}
  \item[Metered data initiative]{\url{https://gitlab.gnome.org/GNOME/Initiatives/-/issues/42}}
  \item[Power-saver initiative]{\url{https://gitlab.gnome.org/GNOME/Initiatives/-/issues/43}}
  \item[Low memory initiative]{\url{https://gitlab.gnome.org/GNOME/Initiatives/-/issues/44}}
	\item[Slide source]{\url{https://gitlab.gnome.org/pwithnall/guadec-power-bandwidth-presentation-2023}}
\end{description}


% Creative commons logos from http://blog.hartwork.org/?p=52
\vfill
\begin{center}
	\includegraphics[scale=0.83]{cc_by_30.pdf}\hspace*{0.95ex}\includegraphics[scale=0.83]{cc_sa_30.pdf}\\[1.5ex]
	{\tiny\textit{Creative Commons Attribution-ShareAlike 4.0 International License}}\\[1.5ex]
	\vspace*{-2.5ex}
\end{center}

\vfill
\begin{center}
	\tiny{Beamer theme: \url{https://gitlab.gnome.org/GNOME/presentation-templates/tree/master/GUADEC/2023}}
\end{center}
\end{frame}

\end{document}
